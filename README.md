# Installationsanleitung

Um das Spiel lokal zu starten, sind folgende Schritte notwendig (Bitte durchgehend die git-Bash verwenden!): 

1. Repo clonen
2. NodeJS installieren (https://nodejs.org/en/)
3. PATH-Variable setzen entsprechend dieser Anleitung: https://gitlab.com/mtlg-framework/mtlg-gameframe/wikis/mtlg/installation#path-variable-setzen
4. In den lokalen Ordner des Repo navigieren, die Konsole öffnen und `npm install ` ausführen
5. `mtlg serve` startet das Spiel