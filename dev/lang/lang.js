
MTLG.lang.define({
  'en': {
    'level1' : 'Level One',
    'level2' : 'Level Two',
    'level3' : 'Level Three',
    'level4' : 'Level Four'
  },
  'de': {
    'level1' : 'Level Eins',
    'level2' : 'Level Zwei',
    'level3' : 'Level Drei',
    'level4' : 'Level Vier'
  }
});
