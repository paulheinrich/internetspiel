function drawMainMenu() {

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
    console.log("Logging in");
    MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  }

  // get stage to add objects
  var stage = MTLG.getStageContainer();
  MTLG.setBackgroundColor("white");

  var title = new createjs.Text("Station 4", "60px Arial", "Black");
  title.textBaseline = 'alphabetic';
  title.textAlign = 'center';
  title.x = 0.5 * MTLG.getOptions().width;
  title.y = 0.2 * MTLG.getOptions().height;

  stage.addChild(title);

  //var buttons = addButtons(["Los geht's"]);

  var los = MTLG.utils.uiElements.addButton({
    text: "Los geht's!",
    sizeX: MTLG.getOptions().width * 0.1,
    sizeY: 70,
  }, function () {
    MTLG.lc.levelFinished({
      nextLevel: "level1_explain"
    });
  });

  los.x = MTLG.getOptions().width / 2;
  los.y = MTLG.getOptions().height / 2;
  stage.addChild(los);

  // add objects to stage
}

function addButtons(levelNames) {
  var res = [];

  for (let i = 0; i < levelNames.length; i++) {
    res.push(
      MTLG.utils.uiElements.addButton({
          text: MTLG.lang.getString(levelNames[i]),
          sizeX: MTLG.getOptions().width * 0.1,
          sizeY: 70,
        },
        function () {
          MTLG.lc.levelFinished({
            nextLevel: levelNames[i]
          });
        }
      )
    );
    res[i].x = MTLG.getOptions().width / 2;
    res[i].y = MTLG.getOptions().height / 2 + 100 * i;
    var stage = MTLG.getStageContainer();
    stage.addChild(res[i]);
  }

  return res;

}