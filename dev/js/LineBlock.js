class LineBlock extends Block {
    constructor(beginX, beginY, sideLength, text, textSize,width,color) {
        if(typeof(width)=="undefined"){
            width = sideLength;
        }
        if(typeof(color)=="undefined"){
            color= "white";
        }
        super(beginX, beginY, width, sideLength);

        this.width = width;
        this.line;
        this.textShape1;
        this.textShape2;
        this.target;
        this.targetContainer;
        this.textSize = textSize;
        this.beginX = beginX;
        this.beginY = beginY;
        this.text = text;
        this.sideLength = sideLength;


        /**
        this.line = new createjs.Shape();
        this.line.graphics.setStrokeStyle(3).beginStroke('black');
        this.line.graphics.moveTo(0, 0);
        this.line.graphics.lineTo(this.endX - this.beginX, this.endY - this.beginY);
        */

        this.textShape1 = MTLG.utils.gfx.getText(this.text, this.textSize + 'px Consolas', 'black');
        this.textShape1.x = this.sideLength*0.05;
        this.textShape1.y = this.sideLength*1.05;

        this.textShape2 = MTLG.utils.gfx.getText(this.text, this.textSize + 'px Consolas', 'black');
        this.textShape2.rotation = 180;
        this.textShape2.x = this.sideLength*(0.95);
        this.textShape2.y = 0;
        if(typeof(width) == "undefined"){
            this.target = new createjs.Shape();
            this.target.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, this.sideLength, this.sideLength);    
        }else{
            this.target = new createjs.Shape();
            this.target.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, width, this.sideLength);
        }
        
        this.boxContainer.addChild(this.textShape1, this.textShape2, this.target);
    }
}