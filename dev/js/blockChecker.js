class BlockChecker {
    constructor(targets) {
        this.targets1 = targets[0]; //array
        this.targets2 = targets[1]; //array
        this.active = 0;
        this.buffer = new Array();
        this.copyCount = 21;
    }

    addCopiedElement(boxC) {
        MTLG.getStageContainer().addChildAt(boxC, this.copyCount);
        this.copyCount++;
    }

    resetActiveBlock() {
        this.active = 0;
    }

    checkIfCorrect(chiffre) {
        //console.log("Dragger: "+chiffre + ", Target: "+this.targets1[this.active].text);
        if (this.targets1[this.active].text == chiffre) {
            this.nextActive();
            return true;
        }
        return false;
    }

    getActiveBlocks() {
        let ret = new Array;
        ret.push(this.targets1[this.active]);
        ret.push(this.targets2[this.active]);
        return ret;
    }

    getActiveNumber() {
        return this.active;
    }

    nextActive() {
        if (this.active + 1 == this.targets1.length) {
            setTimeout(function () {
                let levelButton = MTLG.utils.uiElements.addButton({
                        text: "ins nächste Level",
                        sizeX: 192,
                        sizeY: 70,
                    },
                    function () {
                        MTLG.lc.levelFinished({
                            nextLevel: "level2"
                        });
                    }
                );
                levelButton.x = MTLG.getOptions().width / 2;
                levelButton.y = MTLG.getOptions().height / 2 + 35;
                MTLG.getStageContainer().addChild(levelButton);

                let levelButton2 = MTLG.utils.uiElements.addButton({
                        text: "ins nächste Level",
                        sizeX: 192,
                        sizeY: 70,
                    },
                    function () {
                        MTLG.lc.levelFinished({
                            nextLevel: "level2"
                        });
                    }
                );
                levelButton2.x = MTLG.getOptions().width / 2;
                levelButton2.y = MTLG.getOptions().height / 2 - 35;
                levelButton2.rotation = 180;
                MTLG.getStageContainer().addChild(levelButton2);

            }, 500)
        } else {
            this.active++;
            this.getActiveBlocks()[0].changeColor("yellow");
            this.getActiveBlocks()[1].changeColor("yellow");
        }
    }

    clearBuffer() {
        this.buffer = new Array();
    }

    removeBufferfromDisplay() {
        let stage = MTLG.getStageContainer();
        //console.log(this.buffer);
        for (var i = 0; i < this.buffer.length; i++) {
            stage.removeChild(this.buffer[i]);
        }
        this.clearBuffer();
    }

    bufferPush(p) {
        this.buffer.push(p);
        console.log(this.buffer);
    }

}