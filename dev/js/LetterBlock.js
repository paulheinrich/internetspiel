class LetterBlock extends Block {
    constructor(beginX, beginY, sideLength, textSize,  letter, chiffre,color,checkColor, font, width, _pic, picScale) {
        if(typeof(width)=="undefined"){
            width = sideLength;
        }
        if(typeof(font)=="undefined"){
            font = "Consolas";
        }
        super(beginX, beginY, width, sideLength, color);
        if(font == "Kalam"){
            this.offset = 5;
        }else{
            this.offset = 0;
        }
        this.width = width;
        this.textSize = textSize;
        this.sideLength = sideLength;
        this.ciffreShape;
        this.textShape1;
        this.textShape2;
        this.letter = letter;
        this.chiffre = chiffre;
        this.beginX = beginX;
        this.beginY = beginY;

        this.textShape1 = MTLG.utils.gfx.getText(this.letter, this.textSize + 'px ' + font, 'black');
        this.textShape1.x = 0.5 * this.width;
        this.textShape1.y = 0.505 * this.sideLength+this.offset;
        this.textShape1.textAlign = "center";
        this.textShape2 = MTLG.utils.gfx.getText(this.letter, this.textSize + 'px ' + font, 'black');
        this.textShape2.x = 0.5 * this.width;
        this.textShape2.y = 0.505 * this.sideLength-this.offset;
        this.textShape2.textAlign = "center";
        this.textShape2.rotation = 180;

        if(checkColor || typeof(checkColor) == "undefined"){
            this.line = new createjs.Shape();
            this.line.graphics.setStrokeStyle(3).beginStroke("black");
            this.line.graphics.moveTo(0, this.sideLength*0.5);
            this.line.graphics.lineTo(this.sideLength, this.sideLength*0.5);
            this.line.graphics.endStroke();
        }

        this.pic = MTLG.assets.getBitmap(_pic);
        this.pic.x=0;
        this.pic.y=0;
        this.pic.scaleX=picScale;
        this.pic.scaleY=picScale;

        if(typeof(_pic)=="undefined"){
            this.boxContainer.addChild(this.textShapeRect, this.textShape1, this.textShape2, this.line);
        }else{
            this.boxContainer.addChild(this.pic);
        }
        
        
    }
}