class PictureChecker {
    constructor(targets) {
        this.targets = targets;
        this.active = 0;
    }

    checkIfCorrect(chiffre) {
        if (chiffre == this.active) {
            this.nextActive();
            return true;
        } else {
            return false;
        }
    }

    getActiveBlock() {
        return this.targets[this.active];
    }

    getActiveNumber() {
        return this.active;
    }

    nextActive() {
        let stage = MTLG.getStageContainer();
        if (this.active + 1 == this.targets.length) {

            setTimeout(function () {
                let white = new createjs.Shape();
                white.graphics.beginFill("white").drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
                stage.addChild(white);

                let levelButton = MTLG.utils.uiElements.addButton({
                        text: "Ihr habt die Station erfolgreich abgeschlossen",
                        sizeX: 700,
                        sizeY: 150,
                        bgColor1: "yellow"
                    },
                    function () {
                        MTLG.lc.goToMenu();
                    }
                );
                levelButton.x = MTLG.getOptions().width / 2;
                levelButton.y = MTLG.getOptions().height / 2 + 75;
                stage.addChild(levelButton);

                let levelButton2 = MTLG.utils.uiElements.addButton({
                        text: "Ihr habt die Station erfolgreich abgeschlossen",
                        sizeX: 700,
                        sizeY: 150,
                        bgColor1: "yellow"
                    },
                    function () {
                        MTLG.lc.goToMenu();
                    }
                );
                levelButton2.x = MTLG.getOptions().width / 2;
                levelButton2.y = MTLG.getOptions().height / 2 - 75;
                levelButton2.rotation = 180;
                stage.addChild(levelButton2);

            }, 500)
            stage.removeChild(this.targets[this.active].boxContainer);
        } else {
            stage.removeChild(this.targets[this.active].boxContainer);
            this.active++;
            this.getActiveBlock().changeColor("yellow");
        }


    }
}