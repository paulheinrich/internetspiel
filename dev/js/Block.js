class Block{
    constructor(boxContainerX, boxContainerY, boxContainerLength, boxContainerHeight, color){
        
        if(typeof(color)=="undefined"){
            color = "yellow";
        }

        this.textShapeRect = new createjs.Shape();
        this.textShapeRect.graphics.setStrokeStyle(2).beginFill(color).beginStroke("black").rect(0, 0, boxContainerLength, boxContainerHeight);

        this.boxContainer = new createjs.Container();
        this.boxContainer.x = boxContainerX;
        this.boxContainer.y = boxContainerY;
        this.boxContainer.setBounds(boxContainerX, boxContainerY, boxContainerLength, boxContainerHeight);
        
    }

    changeColor(color){
        //this.textShapeRect.graphics.beginFill(color);
        this.boxContainer.removeChild(this.textShapeRect);
        this.textShapeRect = new createjs.Shape();
        this.textShapeRect.graphics.setStrokeStyle(2).beginFill(color).beginStroke("black").rect(0, 0, this.width, this.sideLength);
        this.boxContainer.addChildAt(this.textShapeRect,0);
    }
}