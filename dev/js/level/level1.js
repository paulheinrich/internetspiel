function level1_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel1();
}

// check wether level 1 is choosen or not
function checkLevel1(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level1") {
        return 1;
    }
    return 0;
}


function drawLevel1() {
    var stage = MTLG.getStageContainer();

    console.log("Level 1 started.");

    // add objects to stage

    MTLG.setBackgroundColor("white");

    var lineBlockArray = createLines(['01001', '01110', '00110', '01111', '10011', '10000', '01000', '00101', '10010', '00101']);

    var chiffre = ['00001', '00010', '00011', '00100', '00101', '00110', '00111', '01000', '01001', '01010', '01011', '01100', '01101', '01110', '01111', '10000', '10001', '10010', '10011', '10100', '10101', '10110', '10111', '11000', '11001', '11010'];

    var alphabetArray = drawAlphabet(chiffre, 13, lineBlockArray, 20);

    let anweisung1 = MTLG.utils.gfx.getText('Hier ist eine Nachricht angekommen und muss zurück in die passenden Buchstaben zurück übersetzt werden.', '28px Consolas', 'black');
    anweisung1.x = 200;
    anweisung1.y = 1000;
    anweisung1.lineHeight = 35;

    let anweisung2 = MTLG.utils.gfx.getText('Hier ist eine Nachricht angekommen und muss zurück in die passenden Buchstaben zurück übersetzt werden.', '28px Consolas', 'black');
    anweisung2.x = 1720;
    anweisung2.y = 80;
    anweisung2.lineHeight = 35;
    anweisung2.rotation = 180;

    stage.addChild(anweisung1, anweisung2);

}

function createLines(text) {
    var stage = MTLG.getStageContainer();
    let ret = new Array();

    let block1 = new Array();
    let block2 = new Array();

    var sideLength = 100;
    var gap = 30;
    var startX = (1920 - (text.length * sideLength + (text.length - 1) * gap)) / 2;

    for (let i = 0; i < text.length; i++) {
        let j = text.length - 1 - i;
        block1[i] = new LineBlock(startX + i * sideLength + i * gap, 580, 100, text[i], 28);
        block2[i] = new LineBlock(startX + j * sideLength + j * gap, 400, 100, text[i], 28);
        stage.addChild(block1[i].boxContainer, block2[i].boxContainer);
    }

    ret.push(block1);
    ret.push(block2);
    return ret;

}

function drawAlphabet(chiffre, number, lines, rate) {
    var stage = MTLG.getStageContainer();
    let alphabet = new Array(26).fill(1).map((_, i) => String.fromCharCode(65 + i));
    let block1 = new Array();
    let block2 = new Array();

    var sideLength = 100;
    var gap = 30;
    var startX = (1920 - (number * sideLength + (number - 1) * gap)) / 2;

    var checker = new BlockChecker(lines);
    checker.getActiveBlocks()[0].changeColor("yellow");
    checker.getActiveBlocks()[1].changeColor("yellow");

    var codes1 = new Array();
    var codes2 = new Array();

    for (let i = 0; i < number * rate; i++) {
        let j = (~~(i / rate));
        block1[i] = new LetterBlock(startX + j * sideLength + j * gap, 810, 100, 60, alphabet[j], chiffre[j]);
        block2[i] = new LetterBlock(startX + j * sideLength + j * gap, 170, 100, 60, alphabet[26 - j - 1], chiffre[26 - j - 1])

        if (i % rate == 0) {
            codes1[j] = MTLG.utils.gfx.getText(chiffre[j], 28 + 'px Consolas', 'black');
            codes1[j].x = startX + j * sideLength + j * gap;
            codes1[j].y = 810 + 100 + 5;
            stage.addChild(codes1[j]);

            codes2[j] = MTLG.utils.gfx.getText(chiffre[26 - j - 1], 28 + 'px Consolas', 'black');
            codes2[j].x = startX + j * sideLength + j * gap + 100;
            codes2[j].y = 170 - 5;
            codes2[j].rotation = 180;
            stage.addChild(codes2[j]);
        }

        assignOnFcts(block1[i], checker, block1[i].chiffre);
        assignOnFcts(block2[i], checker, block2[i].chiffre);

        stage.addChild(block1[i].boxContainer, block2[i].boxContainer);
    }

    return block1;
}

function assignOnFcts(_dragger, checker, draggerChiffre) {
    var stage = MTLG.getStageContainer();
    //let dragger = block1[i].boxContainer;
    //let t = getTarget(lines);

    let dragger = _dragger.boxContainer;

    dragger.on("pressmove", function ({
        localX,
        localY
    }) {
        let t1 = checker.getActiveBlocks()[0];
        let t2 = checker.getActiveBlocks()[1];

        if (t1 != undefined && t2 != undefined) {
            let box1 = t1.target;
            let box2 = t2.target;
            let flag;
            this.x += localX * this.scaleX;
            this.y += localY * this.scaleY;

            if (intersect(dragger, t1.boxContainer)) {
                dragger.alpha = 0.2;
                box1.graphics.clear();
                box1.graphics.setStrokeStyle(3)
                    .beginStroke("#0066A4")
                    .rect(0, 0, 100, 100);
                flag = true;
            } else {
                dragger.alpha = 1;
                box1.graphics.clear();
                box1.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, 100, 100);
                flag = false;
            }

            if (intersect(dragger, t2.boxContainer)) {
                dragger.alpha = 0.2;
                box2.graphics.clear();
                box2.graphics.setStrokeStyle(3)
                    .beginStroke("#0066A4")
                    .rect(0, 0, 100, 100);
            } else {
                if (!flag) {
                    dragger.alpha = 1;
                    box2.graphics.clear();
                    box2.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, 100, 100);
                }
            }
        }

    });

    dragger.on("pressup", function (evt) {
        //changeIndexToFirst(dragger);
        let t1 = checker.getActiveBlocks()[0];
        let t2 = checker.getActiveBlocks()[1];
        let box1 = t1.target;
        let box2 = t2.target;

        let destination1 = t1.boxContainer;
        let destination2 = t2.boxContainer;

        if (intersect(evt.currentTarget, destination1) || intersect(evt.currentTarget, destination2)) {
            if (intersect(evt.currentTarget, destination1)) {
                dragger.x = destination1.x;
                dragger.y = destination1.y;
                dragger.alpha = 1;
                box1.graphics.clear();
                box1.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, 100, 100);
                if (checker.checkIfCorrect(draggerChiffre)) {
                    checker.removeBufferfromDisplay();
                    _dragger.changeColor("green");
                    copy(_dragger, destination2, "green", checker);
                } else {
                    checker.removeBufferfromDisplay();
                    checker.bufferPush(dragger);
                    _dragger.changeColor("red");
                    copy(_dragger, destination2, "red", checker);
                }
            }

            if (intersect(evt.currentTarget, destination2)) {
                dragger.x = destination2.x;
                dragger.y = destination2.y;
                dragger.alpha = 1;
                box2.graphics.clear();
                box2.graphics.setStrokeStyle(2).beginStroke("black").rect(0, 0, 100, 100);
                if (checker.checkIfCorrect(draggerChiffre)) {
                    checker.removeBufferfromDisplay();
                    _dragger.changeColor("green");
                    copy(_dragger, destination1, "green", checker);
                } else {
                    checker.removeBufferfromDisplay();
                    checker.bufferPush(dragger);
                    _dragger.changeColor("red");
                    copy(_dragger, destination1, "red", checker);
                }

            }
        } else {
            stage.removeChild(dragger);
        }

    });

    function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
        var objBounds1 = obj1.getBounds().clone();
        var objBounds2 = obj2.getBounds().clone();

        var pt = obj1.globalToLocal(objBounds2.x - 50, objBounds2.y - 50);

        var h1 = -(objBounds1.height / 2 + objBounds2.height);
        var h2 = objBounds2.width / 2;
        var w1 = -(objBounds1.width / 2 + objBounds2.width);
        var w2 = objBounds2.width / 2;


        if (pt.x > w2 || pt.x < w1) return false;
        if (pt.y > h2 || pt.y < h1) return false;

        return true;
    }
}

function changeIndexToFirst(elem) {
    //var currenIndex = MTLG.getStageContainer().getChildIndex(elem);
    console.log(elem);
    MTLG.getStageContainer().setChildIndex(elem, 0);
    console.log(MTLG.getStageContainer());
}

function copy(elem, dest, col, checker) {
    var n = new LetterBlock(dest.x, dest.y, elem.sideLength, elem.textSize, elem.letter, elem.chiffre, col);
    checker.addCopiedElement(n.boxContainer);
}