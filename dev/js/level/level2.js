function level2_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel2();
}

// check wether level 2 is choosen or not
function checkLevel2(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level2") {
        return 1;
    }
    return 0;
}


function drawLevel2() {
    var stage = MTLG.getStageContainer();

    console.log("Level 2 started.");

    // add objects to stage

    MTLG.setBackgroundColor("white");

    var anweisungText = 'Hier muss die Webseite wieder zusammengesetzt werden. Welche Hintergrundfarbe wird benötigt?';
    let anweisung1 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung1.textAlign = 'center';
    anweisung1.x = 0.5 * MTLG.getOptions().width;
    anweisung1.y = 0.925 * MTLG.getOptions().height;
    anweisung1.lineHeight = 35;
    stage.addChild(anweisung1);

    let anweisung2 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung2.textAlign = 'center';
    anweisung2.x = 0.5 * MTLG.getOptions().width;
    anweisung2.y = 0.075 * MTLG.getOptions().height;
    anweisung2.lineHeight = 35;
    anweisung2.rotation = 180;
    stage.addChild(anweisung2);

    var bitmap = MTLG.assets.getBitmap("img/firefox.png");
    var scaling = 0.6;
    var fireScaling = 1920 / 2265;
    bitmap.x = 1920 * 0.5 * (1 - scaling);
    bitmap.y = 1080 * 0.5 * (1 - scaling);
    bitmap.scaleX = scaling * fireScaling;
    bitmap.scaleY = scaling * fireScaling;

    stage.addChild(bitmap);

    var scaleBauplan = 350 / 1083;

    var bauplan1 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan1.scaleX = scaleBauplan;
    bauplan1.scaleY = scaleBauplan;
    bauplan1.x = 34;
    bauplan1.y = 540 - 0.5 * (scaleBauplan) * 828;
    stage.addChild(bauplan1);

    var bauplan2 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan2.scaleX = scaleBauplan;
    bauplan2.scaleY = scaleBauplan;
    bauplan2.x = 1920 - 34;
    bauplan2.y = 540 + 0.5 * (scaleBauplan)  * 828;
    bauplan2.rotation = 180;
    stage.addChild(bauplan2);

    var orange = new LetterBlock(100, 100, 100, 60, "", "", "orange", false);
    stage.addChild(orange.boxContainer);

    var blue = new LetterBlock(100, 880, 100, 60, "", "", "blue", false);
    stage.addChild(blue.boxContainer);

    var green = new LetterBlock(1720, 100, 100, 60, "", "", "green", false);
    stage.addChild(green.boxContainer);

    var yellow = new LetterBlock(1720, 880, 100, 60, "", "", "yellow", false);
    stage.addChild(yellow.boxContainer);

    var browser = new LineBlock(bitmap.x, bitmap.y, 1367 * scaling * fireScaling, "", 2, 2265 * scaling * fireScaling, "red");
    stage.addChild(browser.boxContainer);

    assignFalseDraggers(blue.boxContainer, browser, 100, 880);
    assignFalseDraggers(green.boxContainer, browser, 1720, 100);
    assignFalseDraggers(yellow.boxContainer, browser, 1720, 880);

    var stage = MTLG.getStageContainer();

    let dragger = orange.boxContainer;

    dragger.on("pressmove", function ({
        localX,
        localY
    }) {
        let t1 = browser;
        this.x += localX * this.scaleX;
        this.y += localY * this.scaleY;

        if (intersect(dragger, t1.boxContainer)) {
            dragger.alpha = 0.2;
        } else {
            dragger.alpha = 1;
        }

    });

    dragger.on("pressup", function (evt) {
        let t1 = browser;
        let box1 = t1.target;

        let destination1 = t1.boxContainer;

        if (intersect(evt.currentTarget, destination1)) {
            dragger.x = destination1.x;
            dragger.y = destination1.y;
            dragger.alpha = 1;
            stage.removeChild(dragger);
            var background = MTLG.assets.getBitmap("img/level3.png");
            var scaling = 0.6;
            var fireScaling = 1; //920/2265;
            background.x = 1920 * 0.5 * (1 - scaling);
            background.y = 1080 * 0.5 * (1 - scaling) + 50;
            background.scaleX = scaling * fireScaling;
            background.scaleY = scaling * fireScaling;
            stage.addChild(background);

            setTimeout(function () {
                let levelButton = MTLG.utils.uiElements.addButton({
                        text: "ins nächste Level",
                        sizeX: 192,
                        sizeY: 70,
                    },
                    function () {
                        MTLG.lc.levelFinished({
                            nextLevel: "level3"
                        });
                    }
                );
                levelButton.x = MTLG.getOptions().width / 2;
                levelButton.y = MTLG.getOptions().height / 2 + 35;
                stage.addChild(levelButton);

                let levelButton2 = MTLG.utils.uiElements.addButton({
                        text: "ins nächste Level",
                        sizeX: 192,
                        sizeY: 70,
                    },
                    function () {
                        MTLG.lc.levelFinished({
                            nextLevel: "level3"
                        });
                    }
                );
                levelButton2.x = MTLG.getOptions().width / 2;
                levelButton2.y = MTLG.getOptions().height / 2 - 35;
                levelButton2.rotation = 180;
                stage.addChild(levelButton2);


            }, 500)
        } else {
            dragger.x = 100;
            dragger.y = 100;
        }

    });

    function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
        var objBounds1 = obj1.getBounds().clone();
        var objBounds2 = obj2.getBounds().clone();

        var pt = obj1.globalToLocal(objBounds2.x, objBounds2.y);

        var h1 = -objBounds2.height;
        var h2 = objBounds1.height;
        var w1 = -objBounds2.width;
        var w2 = objBounds1.width;

        if (pt.x > w2 || pt.x < w1) return false;
        if (pt.y > h2 || pt.y < h1) return false;

        return true;
    }

    function assignFalseDraggers(dragger, browser, originalX, originalY) {
        dragger.on("pressmove", function ({
            localX,
            localY
        }) {
            let t1 = browser;
            this.x += localX * this.scaleX;
            this.y += localY * this.scaleY;

            if (intersect(dragger, t1.boxContainer)) {
                dragger.alpha = 0.2;
            } else {
                dragger.alpha = 1;
            }

        });

        dragger.on("pressup", function (evt) {
            let t1 = browser;
            let box1 = t1.target;

            let destination1 = t1.boxContainer;

            if (intersect(evt.currentTarget, destination1)) {
                showHint();
            }
            dragger.x = originalX;
            dragger.y = originalY;
            dragger.alpha = 1;

        });

        function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
            var objBounds1 = obj1.getBounds().clone();
            var objBounds2 = obj2.getBounds().clone();

            var pt = obj1.globalToLocal(objBounds2.x, objBounds2.y);

            var h1 = -objBounds2.height;
            var h2 = objBounds1.height;
            var w1 = -objBounds2.width;
            var w2 = objBounds1.width;

            if (pt.x > w2 || pt.x < w1) return false;
            if (pt.y > h2 || pt.y < h1) return false;

            return true;
        }
    }

}

function showHint() {
    let fontShape1 = MTLG.utils.gfx.getText("Lest noch einmal genau den Bauplan!", "35px Consolas", "black");
    fontShape1.textAlign = "center";
    fontShape1.x = 1920 / 2;
    fontShape1.y = 1080 / 2 + 100;
    let fontShape2 = MTLG.utils.gfx.getText("Lest noch einmal genau den Bauplan!", "35px Consolas", "black");
    fontShape2.textAlign = "center";
    fontShape2.x = 1920 / 2;
    fontShape2.y = 1080 / 2 - 100;
    fontShape2.rotation = 180;
    MTLG.getStageContainer().addChild(fontShape1, fontShape2);
    setTimeout(function () {
        MTLG.getStageContainer().removeChild(fontShape1, fontShape2);
    }, 3000);
}