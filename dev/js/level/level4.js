function level4_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel4();
}

// check wether level 4 is choosen or not
function checkLevel4(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level4") {
        return 1;
    }
    return 0;
}


function drawLevel4() {
    var stage = MTLG.getStageContainer();

    MTLG.setBackgroundColor("white");

    console.log("Level 4 started.");

    var anweisungText = 'Welche Bilder werden wo benötigt?';
    let anweisung1 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung1.textAlign = 'center';
    anweisung1.x = 0.5 * MTLG.getOptions().width;
    anweisung1.y = 0.925 * MTLG.getOptions().height;
    anweisung1.lineHeight = 35;
    stage.addChild(anweisung1);

    let anweisung2 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung2.textAlign = 'center';
    anweisung2.x = 0.5 * MTLG.getOptions().width;
    anweisung2.y = 0.075 * MTLG.getOptions().height;
    anweisung2.lineHeight = 35;
    anweisung2.rotation = 180;
    stage.addChild(anweisung2);

    gotD1= false;
    gotD2= false;
    gotD3= false;
    gotD4= false;

    var scaleBauplan = 350 / 1083;

    var bauplan1 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan1.scaleX = scaleBauplan;
    bauplan1.scaleY = scaleBauplan;
    bauplan1.x = 34;
    bauplan1.y = 540 - 0.5 * (scaleBauplan) * 828;
    stage.addChild(bauplan1);

    var bauplan2 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan2.scaleX = scaleBauplan;
    bauplan2.scaleY = scaleBauplan;
    bauplan2.x = 1920 - 34;
    bauplan2.y = 540 + 0.5 * (scaleBauplan)  * 828;
    bauplan2.rotation = 180;
    stage.addChild(bauplan2);

    // add objects to stage
    var firefox = MTLG.assets.getBitmap("img/firefox.png");
    var scaling = 0.6;
    var fireScaling = 1920 / 2265;
    firefox.x = 1920 * 0.5 * (1 - scaling);
    firefox.y = 1080 * 0.5 * (1 - scaling);
    firefox.scaleX = scaling * fireScaling;
    firefox.scaleY = scaling * fireScaling;

    stage.addChild(firefox);

    var bitmap = MTLG.assets.getBitmap("img/level4.png");
    var scaling = 0.6;
    bitmap.x = 1920 * 0.5 * (1 - scaling);
    bitmap.y = 1080 * 0.5 * (1 - scaling)+50;
    bitmap.scaleX = scaling;
    bitmap.scaleY = scaling;

    stage.addChild(bitmap);
    
    var length=180;
    var buffer=5;

    var texts = new Array();

    function getText(text){
        return MTLG.utils.gfx.getText(text, "16px Consolas", "black")
    }

    var orange = new LetterBlock(100, 100, length, 60, "", 0, "orange", false, null ,length, "img/fragezeichen.jpg", 180/200 );
    stage.addChild(orange.boxContainer);
    texts[0]=getText("fragezeichen.jpg");
    texts[0].y=orange.boxContainer.y+length+buffer;
    texts[0].x=orange.boxContainer.x;
    texts[1]=getText("fragezeichen.jpg");
    texts[1].y=orange.boxContainer.y-buffer;
    texts[1].x=orange.boxContainer.x+length;
    texts[1].rotation=180;

    var blue = new LetterBlock(100, 780, length, 60, "", 1, "blue", false, null, length, "img/pfeil.jpg", 180/200 );
    stage.addChild(blue.boxContainer);
    texts[2]=getText("pfeil.jpg");
    texts[2].y=blue.boxContainer.y+length+buffer;
    texts[2].x=blue.boxContainer.x;
    texts[3]=getText("pfeil.jpg");
    texts[3].y=blue.boxContainer.y-buffer;
    texts[3].x=blue.boxContainer.x+length;
    texts[3].rotation=180;

    var green = new LetterBlock(1620, 100, length, 60, "", 2, "green", false, null ,length, "img/cyber-glass.jpg", 180/200);
    stage.addChild(green.boxContainer);
    texts[4]=getText("brille.jpg");
    texts[4].y=green.boxContainer.y+length+buffer;
    texts[4].x=green.boxContainer.x;
    texts[5]=getText("brille.jpg");
    texts[5].y=green.boxContainer.y-buffer;
    texts[5].x=green.boxContainer.x+length;
    texts[5].rotation=180;

    var white = new LetterBlock(1620, 780, length, 60, "", 3, "white", false, null ,length, "img/haus.jpg", 180/200);
    stage.addChild(white.boxContainer);
    texts[6]=getText("tuer.jpg");
    texts[6].y=white.boxContainer.y+length+buffer;
    texts[6].x=white.boxContainer.x;
    texts[7]=getText("tuer.jpg");
    texts[7].y=white.boxContainer.y-buffer;
    texts[7].x=white.boxContainer.x+length;
    texts[7].rotation=180;

    for(var i =0;i<texts.length;i++){
        stage.addChild(texts[i]);
    }

    var dest1 = new LineBlock(545, 430, length, "", 2, length);
    stage.addChild(dest1.boxContainer);

    var dest2 = new LineBlock(880, 430, length, "", 2, length);
    stage.addChild(dest2.boxContainer);

    var dest3 = new LineBlock(545, 665, length, "", 2, length);
    stage.addChild(dest3.boxContainer);

    var dest4 = new LineBlock(880, 665, length, "", 2, length);
    stage.addChild(dest4.boxContainer);

    var checker = new PictureChecker([dest1,dest2, dest3, dest4]);
    checker.getActiveBlock().changeColor("yellow");

    assignOnFcts(orange, checker, orange.chiffre, 100,100);
    assignOnFcts(blue, checker, blue.chiffre, 100,780);
    assignOnFcts(green, checker, green.chiffre, 1620,100);
    assignOnFcts(white, checker, white.chiffre, 1620,780);

    function assignOnFcts(_dragger, checker, draggerChiffre, originalX, originalY) {
        var stage = MTLG.getStageContainer();
        //let dragger = block1[i].boxContainer;
        //let t = getTarget(lines);
    
        let dragger = _dragger.boxContainer;
    
        dragger.on("pressmove", function ({
            localX,
            localY
        }) {
            let t1 = checker.getActiveBlock();
            let box1 = t1.target;
            this.x += localX * this.scaleX;
            this.y += localY * this.scaleY;
    
            if (intersect(dragger, t1.boxContainer)) {
                dragger.alpha = 0.2;
            } else {
                dragger.alpha = 1;
            }
    
        });
    
        dragger.on("pressup", function (evt) {
            //changeIndexToFirst(dragger);
            let t1 = checker.getActiveBlock();
            let box1 = t1.target;
    
            let destination1 = t1.boxContainer;
    
            if (intersect(evt.currentTarget, destination1)) {
                
                
                dragger.alpha = 1;
                
                if (checker.checkIfCorrect(draggerChiffre)) {
                    dragger.x = destination1.x;
                    dragger.y = destination1.y;
                    
                } else {
                    dragger.x = originalX;
                    dragger.y = originalY;
                }
            
    
            } else {
                dragger.x = originalX;
                dragger.y = originalY;
            }
    
        });
    
        function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
            var objBounds1 = obj1.getBounds().clone();
            var objBounds2 = obj2.getBounds().clone();

            var pt = obj1.globalToLocal(objBounds2.x, objBounds2.y);

            var h1 = -objBounds2.height;
            var h2 = objBounds1.height;
            var w1 = -objBounds2.width;
            var w2 = objBounds1.width;

            if (pt.x > w2 || pt.x < w1) return false;
            if (pt.y > h2 || pt.y < h1) return false;

            return true;
        }
    }

}

