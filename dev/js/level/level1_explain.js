function level1_explain_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel1_explain();
}

// check wether level 1 is choosen or not
function checkLevel1_explain(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level1_explain") {
        return 1;
    }
    return 0;
}

function drawLevel1_explain() {
    var butOk = new Array();
    var but = new Array();
    butOk.push(false);
    butOk.push(true);

    var stage = MTLG.getStageContainer();
    var text = "Nicht nur Bilder werden im Internet transportiert,\nsondern auch E-Mails und Webseiten. Also müssen diese auch\nin Zahlen umgewandelt werden.";

    var title = new createjs.Text(text, "40px Consolas", "Black");
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 0.5 * MTLG.getOptions().width;
    title.y = 0.25 * MTLG.getOptions().height;
    title.lineHeight = 45;
    title.rotation = 180;

    //stage.addChild(title);

    var title2 = new createjs.Text(text, "40px Consolas", "Black");
    title2.textBaseline = 'alphabetic';
    title2.textAlign = 'center';
    title2.x = 0.5 * MTLG.getOptions().width;
    title2.y = 0.75 * MTLG.getOptions().height;
    title2.lineHeight = 45;

    stage.addChild(title2);

    but.push(MTLG.utils.uiElements.addButton({
        text: "Alles klar!",
        sizeX: MTLG.getOptions().width * 0.1,
        sizeY: 70
    }, () => {
        butOk[0] = true;
        stage.removeChild(but[0]);
        checkLevelFinished();
    }))
    but[0].x = 0.5 * MTLG.getOptions().width;
    but[0].y = 0.9 * MTLG.getOptions().height;

    but.push(MTLG.utils.uiElements.addButton({
        text: "Alles klar!",
        sizeX: MTLG.getOptions().width * 0.1,
        sizeY: 70
    }, () => {
        butOk[1] = true;
        stage.removeChild(but[1]);
        checkLevelFinished();
    }));
    but[1].x = 0.5 * MTLG.getOptions().width;
    but[1].y = 0.1 * MTLG.getOptions().height;
    but[1].rotation = 180;

    stage.addChild(but[0]);
    //stage.addChild(but[1]);

    function checkLevelFinished() {
        if (butOk[0] && butOk[1]) {
            MTLG.lc.levelFinished({
                nextLevel: "level1_explain2"
            });
        }
    }
}