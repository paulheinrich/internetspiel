function level1_explain2_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel1_explain2();
}

// check wether level 1 is choosen or not
function checkLevel1_explain2(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level1_explain2") {
        return 1;
    }
    return 0;
}

function drawLevel1_explain2() {
    var state = 0;
    var stage = MTLG.getStageContainer();
    var email = MTLG.assets.getBitmap("img/email.jpg");
    email.x = 200;
    email.y = 200;
    email.scaleX = 400 / 491;
    email.scaleY = 400 / 491;
    stage.addChild(email);

    var bauplan_email = MTLG.assets.getBitmap("img/bauplan_email.jpg");
    bauplan_email.x = 760;
    bauplan_email.y = 500;
    bauplan_email.scaleX = 300 / 640;
    bauplan_email.scaleY = 300 / 640;
    //stage.addChild(bauplan_email);

    var title = new createjs.Text("1010 0001 0011 ...", "40px Consolas", "Black");
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 1500;
    title.y = 350;
    title.lineHeight = 35;

    //stage.addChild(title)

    var LINE_RADIUS = 10; // the short radius of the "line box"
    var ARROWHEAD_RADIUS = 25; // the arrowhead radius;
    var ARROWHEAD_DEPTH = 30;

    var start1 = new createjs.Point(500, 500);
    var end1 = new createjs.Point(760, 600);
    var arrow1 = new createjs.Shape();
    var arrowSize1 = _distance(start1, end1);
    var arrowRotation1 = _angle(start1, end1);
    arrow1.graphics.s("black")
        .f("green")
        .mt(0, 0)
        .lt(0, LINE_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, LINE_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, ARROWHEAD_RADIUS)
        .lt(arrowSize1, 0)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, -ARROWHEAD_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, -LINE_RADIUS)
        .lt(0, -LINE_RADIUS)
        .lt(0, 0)
        .es();
    arrow1.x = start1.x;
    arrow1.y = start1.y;
    arrow1.alpha = 1;
    arrow1.rotation = arrowRotation1;

    var start2 = new createjs.Point(1260, 600);
    var end2 = new createjs.Point(1520, 500);
    var arrow2 = new createjs.Shape();
    var arrowSize2 = _distance(start2, end2);
    var arrowRotation2 = _angle(start2, end2);
    arrow2.graphics.s("black")
        .f("green")
        .mt(0, 0)
        .lt(0, LINE_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, LINE_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, ARROWHEAD_RADIUS)
        .lt(arrowSize2, 0)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, -ARROWHEAD_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, -LINE_RADIUS)
        .lt(0, -LINE_RADIUS)
        .lt(0, 0)
        .es();
    arrow2.x = start2.x;
    arrow2.y = start2.y;
    arrow2.alpha = 1;
    arrow2.rotation = arrowRotation2;

    but = MTLG.utils.uiElements.addButton({
        text: "Alles klar!",
        sizeX: MTLG.getOptions().width * 0.1,
        sizeY: 70
    }, () => {
        switch (state) {
            case 0:
                stage.addChild(bauplan_email, arrow1);
                break;

            case 1:
                stage.addChild(title, arrow2);
                break;

            case 2:
                stage.removeChild(title, bauplan_email, email, arrow1, arrow2);
                var aufforderung = new createjs.Text("Die Hälfte von euch kann nun auf die andere Seite gehen.", "40px Consolas", "Black");
                aufforderung.textBaseline = 'alphabetic';
                aufforderung.textAlign = 'center';
                aufforderung.x = MTLG.getOptions().width * 0.5;
                aufforderung.y = MTLG.getOptions().height * 0.7;
                aufforderung.lineHeight = 35;
                stage.addChild(aufforderung);

                setTimeout(function () {
                    let levelButton = MTLG.utils.uiElements.addButton({
                            text: "ins nächste Level",
                            sizeX: 192,
                            sizeY: 70,
                        },
                        function () {
                            MTLG.lc.levelFinished({
                                nextLevel: "level1"
                            });
                        }
                    );
                    levelButton.x = MTLG.getOptions().width / 2;
                    levelButton.y = MTLG.getOptions().height / 2 + 35;
                    stage.addChild(levelButton);

                    let levelButton2 = MTLG.utils.uiElements.addButton({
                            text: "ins nächste Level",
                            sizeX: 192,
                            sizeY: 70,
                        },
                        function () {
                            MTLG.lc.levelFinished({
                                nextLevel: "level1"
                            });
                        }
                    );
                    levelButton2.x = MTLG.getOptions().width / 2;
                    levelButton2.y = MTLG.getOptions().height / 2 - 35;
                    levelButton2.rotation = 180;
                    stage.addChild(levelButton2);


                }, 500)
                stage.removeChild(but);
                break;

            default:
                break;
        }
        state++;
    })

    but.x = 0.5 * MTLG.getOptions().width;
    but.y = 0.9 * MTLG.getOptions().height;

    stage.addChild(but);

    function _distance(from, to) {
        var dx = from.x - to.x;
        var dy = from.y - to.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    function _angle(p1, p2) {
        return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
    }
}