function level2_explain_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel2_explain();
}

// check wether level 1 is choosen or not
function checkLevel2_explain(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level2_explain") {
        return 1;
    }
    return 0;
}

function drawLevel2_explain() {
    var state = 0;
    var stage = MTLG.getStageContainer();
    var webseite = MTLG.assets.getBitmap("img/schritt2.jpg");
    webseite.x = 200;
    webseite.y = 200;
    webseite.scaleX = 400 / 1080;
    webseite.scaleY = 400 / 1080;
    stage.addChild(webseite);

    var bauplan_webseite = MTLG.assets.getBitmap("img/bauplan_webseite.jpg");
    bauplan_webseite.x = 760;
    bauplan_webseite.y = 500;
    bauplan_webseite.scaleX = 400 / 648;
    bauplan_webseite.scaleY = 400 / 648;
    //stage.addChild(bauplan_webseite);

    var title = new createjs.Text("1010 0001 0011 ...", "40px Consolas", "Black");
    title.textBaseline = 'alphabetic';
    title.textAlign = 'center';
    title.x = 1500;
    title.y = 350;
    title.lineHeight = 35;

    //stage.addChild(title)

    var LINE_RADIUS = 10; // the short radius of the "line box"
    var ARROWHEAD_RADIUS = 25; // the arrowhead radius;
    var ARROWHEAD_DEPTH = 30;

    var start1 = new createjs.Point(500, 500);
    var end1 = new createjs.Point(760, 600);
    var arrow1 = new createjs.Shape();
    var arrowSize1 = _distance(start1, end1);
    var arrowRotation1 = _angle(start1, end1);
    arrow1.graphics.s("black")
        .f("green")
        .mt(0, 0)
        .lt(0, LINE_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, LINE_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, ARROWHEAD_RADIUS)
        .lt(arrowSize1, 0)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, -ARROWHEAD_RADIUS)
        .lt(arrowSize1 - ARROWHEAD_DEPTH, -LINE_RADIUS)
        .lt(0, -LINE_RADIUS)
        .lt(0, 0)
        .es();
    arrow1.x = start1.x;
    arrow1.y = start1.y;
    arrow1.alpha = 1;
    arrow1.rotation = arrowRotation1;

    var start2 = new createjs.Point(1260, 600);
    var end2 = new createjs.Point(1520, 500);
    var arrow2 = new createjs.Shape();
    var arrowSize2 = _distance(start2, end2);
    var arrowRotation2 = _angle(start2, end2);
    arrow2.graphics.s("black")
        .f("green")
        .mt(0, 0)
        .lt(0, LINE_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, LINE_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, ARROWHEAD_RADIUS)
        .lt(arrowSize2, 0)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, -ARROWHEAD_RADIUS)
        .lt(arrowSize2 - ARROWHEAD_DEPTH, -LINE_RADIUS)
        .lt(0, -LINE_RADIUS)
        .lt(0, 0)
        .es();
    arrow2.x = start2.x;
    arrow2.y = start2.y;
    arrow2.alpha = 1;
    arrow2.rotation = arrowRotation2;

    but = MTLG.utils.uiElements.addButton({
        text: "Alles klar!",
        sizeX: MTLG.getOptions().width * 0.1,
        sizeY: 70
    }, () => {
        switch (state) {
            case 0:
                stage.addChild(bauplan_webseite, arrow1);
                break;

            case 1:
                stage.addChild(title, arrow2);
                break;

            case 2:
                stage.removeChild(title, bauplan_webseite, webseite, arrow1, arrow2);


            case 3:
                MTLG.lc.levelFinished({
                    nextLevel: "level1"
                });
                break;

            default:
                break;
        }
        state++;
    })

    but.x = 0.5 * MTLG.getOptions().width;
    but.y = 0.9 * MTLG.getOptions().height;

    stage.addChild(but);

    function _distance(from, to) {
        var dx = from.x - to.x;
        var dy = from.y - to.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    function _angle(p1, p2) {
        return Math.atan2(p2.y - p1.y, p2.x - p1.x) * 180 / Math.PI;
    }
}