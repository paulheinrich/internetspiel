function level3_init() {
    // initialize level 1

    console.log("This players are logged in:");
    for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
        console.log(MTLG.getPlayer(i));
    }

    console.log("Thie are the available game options:");
    console.log(MTLG.getOptions());

    // esit the game.settings.js
    console.log("Thie are the available game settings:");
    console.log(MTLG.getSettings());

    drawLevel3();
}

// check wether level 3 is choosen or not
function checkLevel3(gameState) {
    if (gameState && gameState.nextLevel && gameState.nextLevel == "level3") {
        return 1;
    }
    return 0;
}


function drawLevel3() {
    var stage = MTLG.getStageContainer();

    console.log("Level 3 started.");

    // add objects to stage

    MTLG.setBackgroundColor("white");

    var anweisungText = 'Welche Überschrift wird benötigt?';
    let anweisung1 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung1.textAlign = 'center';
    anweisung1.x = 0.5 * MTLG.getOptions().width;
    anweisung1.y = 0.925 * MTLG.getOptions().height;
    anweisung1.lineHeight = 35;
    stage.addChild(anweisung1);

    let anweisung2 = new createjs.Text(anweisungText, '28px Consolas', 'red');
    anweisung2.textAlign = 'center';
    anweisung2.x = 0.5 * MTLG.getOptions().width;
    anweisung2.y = 0.075 * MTLG.getOptions().height;
    anweisung2.lineHeight = 35;
    anweisung2.rotation = 180;
    stage.addChild(anweisung2);

    var firefox = MTLG.assets.getBitmap("img/firefox.png");
    var scaling = 0.6;
    var fireScaling = 1920 / 2265;
    firefox.x = 1920 * 0.5 * (1 - scaling) + 1920 * scaling;
    firefox.y = 1080 * 0.5 * (1 - scaling) + 1080 * scaling;
    firefox.scaleX = scaling * fireScaling;
    firefox.scaleY = scaling * fireScaling;
    firefox.rotation = 180;

    stage.addChild(firefox);

    var scaleBauplan = 350 / 1083;

    var bauplan1 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan1.scaleX = scaleBauplan;
    bauplan1.scaleY = scaleBauplan;
    bauplan1.x = 34;
    bauplan1.y = 540 - 0.5 * (scaleBauplan) * 828;
    stage.addChild(bauplan1);

    var bauplan2 = MTLG.assets.getBitmap("img/bauplan_webseite2.jpg");
    bauplan2.scaleX = scaleBauplan;
    bauplan2.scaleY = scaleBauplan;
    bauplan2.x = 1920 - 34;
    bauplan2.y = 540 + 0.5 * (scaleBauplan)  * 828;
    bauplan2.rotation = 180;
    stage.addChild(bauplan2);

    var bitmap = MTLG.assets.getBitmap("img/ohneUeberschrift.jpg");
    var scaling = 0.6;
    bitmap.x = 1920 * 0.5 * (1 - scaling) + 1920 * scaling;
    bitmap.y = 1080 * 0.5 * (1 - scaling) - 50 + 1080 * scaling;
    bitmap.scaleX = scaling;
    bitmap.scaleY = scaling;
    bitmap.rotation = 180;

    stage.addChild(bitmap);

    texts = ["Die Seite mit der Maus", "Maus-Seite", "Die Sendung mit der Maus", "WDR-Maus-Seite"];

    var posOffsetX = 50;
    var posOffsetY = 50;
    var length = 350;
    var height = 100;
    xs = [posOffsetX, 1920 - posOffsetX - length, posOffsetX, 1920 - posOffsetX - length];
    ys = [posOffsetY, posOffsetY, 1080 - posOffsetY - height, 1080 - posOffsetY - height];

    let text = new Array();

    let rect = new createjs.Shape();
    rect.graphics.setStrokeStyle(2).beginStroke("black").rect(450, 675, 400, 100);
    //stage.addChild(rect);

    let textField = new LineBlock(450, 675, 100, "", 2, 400);
    //textField.changeColor("red");
    stage.addChild(textField.boxContainer);

    for (let i = 0; i < 4; i++) {
        text[i] = new LetterBlock(xs[i], ys[i], 100, 30, texts[i], "", "white", false, "Kalam", 350);
        stage.addChild(text[i].boxContainer);
        if (i != 0) {
            assignDraggers(text[i].boxContainer, textField, xs[i], ys[i]);
        } else {
            let dragger = text[i].boxContainer;
            let browser = textField;
            dragger.on("pressmove", function ({
                localX,
                localY
            }) {
                let t1 = browser;
                this.x += localX * this.scaleX;
                this.y += localY * this.scaleY;

                if (intersect(dragger, t1.boxContainer)) {
                    dragger.alpha = 0.2;
                } else {
                    dragger.alpha = 1;
                }

            });

            dragger.on("pressup", function (evt) {
                let t1 = browser;
                let box1 = t1.target;

                let destination1 = t1.boxContainer;

                if (intersect(evt.currentTarget, destination1)) {
                    dragger.x = destination1.x;
                    dragger.y = destination1.y;
                    dragger.alpha = 1;
                    stage.removeChild(dragger);
                    var background = MTLG.assets.getBitmap("img/mitUberschrift.jpg");
                    var scaling = 0.6;
                    var fireScaling = 1; //920/2265;
                    background.x = 1920 * 0.5 * (1 - scaling) + 1920 * scaling;
                    background.y = 1080 * 0.5 * (1 - scaling) - 50 + 1080 * scaling;
                    background.scaleX = scaling * fireScaling;
                    background.scaleY = scaling * fireScaling;
                    background.rotation=180;
                    stage.removeChild(bitmap);
                    stage.addChild(background);
                    
                    setTimeout(function () {
                        let levelButton = MTLG.utils.uiElements.addButton({
                                text: "ins nächste Level",
                                sizeX: 192,
                                sizeY: 70,
                            },
                            function () {
                                MTLG.lc.levelFinished({
                                    nextLevel: "level4"
                                });
                            }
                        );
                        levelButton.x = MTLG.getOptions().width / 2;
                        levelButton.y = MTLG.getOptions().height / 2 + 35;
                        stage.addChild(levelButton);

                        let levelButton2 = MTLG.utils.uiElements.addButton({
                                text: "ins nächste Level",
                                sizeX: 192,
                                sizeY: 70,
                            },
                            function () {
                                MTLG.lc.levelFinished({
                                    nextLevel: "level4"
                                });
                            }
                        );
                        levelButton2.x = MTLG.getOptions().width / 2;
                        levelButton2.y = MTLG.getOptions().height / 2 - 35;
                        levelButton2.rotation = 180;
                        stage.addChild(levelButton2);


                    }, 500)
                } else {
                    dragger.x = xs[i];
                    dragger.y = ys[i];
                }

            });

            function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
                var objBounds1 = obj1.getBounds().clone();
                var objBounds2 = obj2.getBounds().clone();

                var pt = obj1.globalToLocal(objBounds2.x, objBounds2.y);

                var h1 = -objBounds2.height;
                var h2 = objBounds1.height;
                var w1 = -objBounds2.width;
                var w2 = objBounds1.width;

                if (pt.x > w2 || pt.x < w1) return false;
                if (pt.y > h2 || pt.y < h1) return false;

                return true;
            }
        }
    }
}

function assignDraggers(dragger, browser, originalX, originalY) {
    dragger.on("pressmove", function ({
        localX,
        localY
    }) {
        let t1 = browser;
        this.x += localX * this.scaleX;
        this.y += localY * this.scaleY;

        if (intersect(dragger, t1.boxContainer)) {
            dragger.alpha = 0.2;
        } else {
            dragger.alpha = 1;
        }

    });

    dragger.on("pressup", function (evt) {
        let t1 = browser;
        let box1 = t1.target;

        let destination1 = t1.boxContainer;

        if (intersect(evt.currentTarget, destination1)) {
            showHint();
        }
        dragger.x = originalX;
        dragger.y = originalY;
        dragger.alpha = 1;

    });

    function intersect(obj1, obj2) { //obj1 = dragger, obj2 = target
        var objBounds1 = obj1.getBounds().clone();
        var objBounds2 = obj2.getBounds().clone();

        var pt = obj1.globalToLocal(objBounds2.x, objBounds2.y);

        var h1 = -objBounds2.height;
        var h2 = objBounds1.height;
        var w1 = -objBounds2.width;
        var w2 = objBounds1.width;

        if (pt.x > w2 || pt.x < w1) return false;
        if (pt.y > h2 || pt.y < h1) return false;

        return true;
    }
}